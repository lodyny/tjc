
# TJC - 1st tournament
## Information

|Tournament|Game|Probability to Continue|Max iterations|Opponent
|-------|----------|-----|---|-
|1|Prisoner's Dilemma|P1 = 1.0|I = 20|Round Robin
|2|Prisoner's Dilemma|P1 < 1.0 (given with challenge)|I = **∞**|Round Robin
|3|Prisoner's Dilemma|P1 < 1.0 (given with challange)|I > 1 (given with challange)|Round Robin

![Prisoner's Dilemma schema](https://bitbucket.org/lodyny/tjc/raw/a1a3aff3f15bcb4a645e21515b9c4f069e4f55ca/PD.png)

This are the two java implementations, one for the first tournament where the max iterations is defined and the second implementation for the others two tournaments.
Link for the implementations: <link>[BitBucket](https://bitbucket.org/lodyny/tjc/src/master/)</link>

## Tit4Tat

Tit for tat is a well known game-theory strategy where as long as the other player don't betray us, we keep cooperating.
This was the base idea behind both implementations developed.


### NeroT4TLimitedRounds.java

<u>Used in the first tournament.</u>
This is based on Tit for that meaning that we are going to cooperate until the other player decide to betray us.
Since the first tournament max iterations is known (20 iterations), one modification was made that check if we are in the last round. If we are in the last round, we betray the other player expecting that the other player is using the standard Tit4Tat strategy, allowing us to get some advantage.

### NeroT4TUnlimited.java

<u>Used in the second and third tournament.</u>
This is the pure Tit4Tat strategy, since we don't know the number of iterations we keep cooperating with the other player until he betray us and we start to defect.

## Made by César Nero - 58659
package play;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import play.exception.InvalidStrategyException;

public class NeroT4TUnlimited  extends Strategy {

	@Override
	public void execute() throws InterruptedException {
		while(!this.isTreeKnown()) {
			System.err.println("Waiting for game tree to become available.");
			Thread.sleep(1000);
		}
		
		List<String> lastTurn = new ArrayList<>();
		boolean betrayed = false;
		
		while(true) {
			PlayStrategy myStrategy = this.getStrategyRequest();
			if(myStrategy == null) //Game was terminated by an outside event
				break;	
			boolean playComplete = false;
						
			while(! playComplete ) {
				lastTurn.clear();
				if(myStrategy.getFinalP1Node() != -1) {
					GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
					GameNode fatherP1 = null;
					if(finalP1 != null) {
						try {
							fatherP1 = finalP1.getAncestor();
						} catch (GameNodeDoesNotExistException e) {
							e.printStackTrace();
						}
						lastTurn.add(finalP1.getLabel());
				}
				}			
				if(myStrategy.getFinalP2Node() != -1) {
					GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
					GameNode fatherP2 = null;
					if(finalP2 != null) {
						try {
							fatherP2 = finalP2.getAncestor();
						} catch (GameNodeDoesNotExistException e) {
							e.printStackTrace();
						}
						lastTurn.add(finalP2.getLabel());
}
				}	
				
				
				// Checking if got betrayed.
				if (!betrayed) {
					for (String string : lastTurn) {
						if (string.toUpperCase().contains("DEFECT"))
							betrayed = true;
					}
				}

				for (String string : lastTurn) {
					System.out.println("V: " + string);
				}
				
				if (betrayed) {
					double[] play = {new Double(0), new Double(1), new Double(0), new Double(1)};
					applyStrategy(myStrategy, play);
				}else {
					double[] play = {new Double(1), new Double(0), new Double(1), new Double(0)};
					applyStrategy(myStrategy, play);
				}
				
				try{
					this.provideStrategy(myStrategy);
					playComplete = true;
				} catch (InvalidStrategyException e) {
					System.err.println("Invalid strategy: " + e.getMessage());;
					e.printStackTrace(System.err);
				} 
			}
		}
		
	}
	
	public void applyStrategy(PlayStrategy myStrategy, double[] moves) {
		Iterator<String> keys = myStrategy.keyIterator();
		for(int i = 0; i < moves.length; i++) {
			if(!keys.hasNext()) {
				System.err.println("PANIC: Strategy structure does not match the game.");
				return;
			}
			myStrategy.put(keys.next(), moves[i]);
		}
	}
	
}
